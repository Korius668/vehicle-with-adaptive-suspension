#include <hFramework.h>
#include <DistanceSensor.h>
#include <MPU9250.h>
#include <stdlib.h>

using namespace hModules;

char c;
int Mot1enc, Mot2enc, Mot3enc, Mot4enc;
    
int16_t ax, ay, az, gx, gy, gz;
int32_t gcx, gcy, gcz;
void encoder()
{
    while (true)
    {   
        //system ("CLS");
        Mot1enc = hMot1.getEncoderCnt(); 
        Mot2enc = hMot2.getEncoderCnt(); 
        Mot3enc = hMot3.getEncoderCnt();
        Mot4enc = hMot4.getEncoderCnt();

        printf("\n");
        Serial.printf("encoder ticks: %5d %5d %5d %5d\r\n",Mot1enc,Mot2enc,Mot3enc,Mot4enc);
        // printf("ax %6d ay %6d az %6d \r\n", ax, ay, az);
        printf("gx %6d gy %6d gz %6d \r\n", gx, gy, gz);
        printf("gcx %6d gcy %6d gcz %6d \r\n", gcx, gcy, gcz);
        hLED1.toggle();
        sys.delay(500);
    }
}

bool stop (int dist)
{
    if(dist<100)    return 1;
    else            return 0;
}

bool ground (int dist)
{
    if(dist<50)    return 1;
    else            return 0;
}

void klawiatura()
{
    for(;;)
    {
    c = getchar();
    printf("c= %c \n",c);
    }
}

/*void zyroskop()
{
    
    uint32_t time1, time2;
    gcx = gcy = gcz = 0;
    time1 = sys.getRefTime();
    sys.setLogDev(&Serial);
    MPU9250 mpu(hSens1);
    mpu.init();
    mpu.enableInterrupt();
    
    for(;;)
    {
        if (mpu.process())
        {
        time2=sys.getRefTime();
        mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
        gcx= gcx + gx*(time2-time1);
        gcy= gcy + gy*(time2-time1);
        gcz= gcz + gz*(time2-time1);
        printf("gx %6d gy %6d gz %6d \r\n", gx, gy, gz);
        printf("gcx %f gcy %f gcz %f \r\n", gcx, gcy, gcz);
        printf("time1 %f time2 %6d \r\n", time1, time2);
        time1 = sys.getRefTime();
        
        sys.delay(100);
        }
    }
}
*/

void hMain()
{
    Mot1enc= Mot2enc= Mot3enc= Mot4enc= 0;

    int cnt, kir, moc, moc3, moc4, wysp, wyst, df, db, dg;
        cnt= kir= moc= moc3= moc4= wysp= wyst= df= db= dg=0;                 

    bool poz, str, sp, sd, ws, stop2;
         poz= str= sp= sd= ws= stop2=0;   
 
    DistanceSensor ds_f(hSens2);                // ds_f - Distance sensor front - Sensor odleglosci przod
    DistanceSensor ds_b(hSens3);                // ds_b - Distance sensor back - Sensor odleglosci tyl
    DistanceSensor ds_g(hSens4);                // ds_g - Distance sensor ground - Sensor odleglosci podłogi
    
    sys.setLogDev(&Serial);
    // SILNIKI
    hMot1.setEncoderPolarity(Polarity::Normal);
    hMot1.setMotorPolarity(Polarity::Reversed);
    hMot2.setEncoderPolarity(Polarity::Normal);
    hMot2.setMotorPolarity(Polarity::Reversed);
    hMot3.setEncoderPolarity(Polarity::Normal);
    hMot3.setMotorPolarity(Polarity::Reversed);
    hMot4.setEncoderPolarity(Polarity::Normal);
    hMot4.setMotorPolarity(Polarity::Reversed);
    //  PROCESY   
    sys.taskCreate(&klawiatura);
    //sys.taskCreate(&encoder);
   // sys.taskCreate(&zyroskop);

    while (true)
    {   
        df = ds_f.getDistance();    //  Zbiera dystans przod
        db = ds_b.getDistance();    //  Zbiera dystans tyl
        dg = ds_g.getDistance();    //  Zbiera dystans podlogi

        sd = hBtn1.isPressed();         // sd - Speed Down - Zwolnienie napedu
        sp = hBtn2.isPressed();         // sp - Speed Up - Przyspieszenie napedu
        
        //printf("\n");
       // printf("sp = %d / sd = %d \r\n",sp,sd);

///////////////////////////////////////////////////////////////////////////////////////
        //PRĘDKOŚĆ
        if(sp==1)
        {
            moc=moc+100;
        }
        if(sd==1)
        {
            moc=moc-100;
        }

        //printf("\n");
        //Serial.printf("df = %d / db = %d \r\n",df,db);
/////////////////////////////////////////////////////////////////////////////////////
        //ŻYROSKOP
                  

            if(poz)
            {   
                if(gy>-2400 && gy<2400)
                {
                    if(gcy<-20)
                    {
                        hMot3.setPower(300);
                        hMot4.setPower(-300);
                        printf("przod");
                    }
                    else if(gcy>20)
                    {
                        hMot3.setPower(-300);
                        hMot4.setPower(300);
                        printf("tyl");
                    }        
                    else
                    {
                        hMot3.setPower(0);
                        hMot4.setPower(0);
                    }
                }
            }
            else
            {
                hMot3.setPower(0);
                hMot4.setPower(0);
            }
/////////////////////////////////////////////////////////////////////////////////////
       /* 
        if (hExt1.serial.available() > 0)
        {
            c = (char) hExt1.serial.getch();
        }
*/
        

        switch (c)
        {
            /*case 'R':
            case 'r':
            cnt= moc= moc3= moc4= df= db= dg=0;
            Mot1enc= Mot2enc= Mot3enc= Mot4enc= gcx= gcy= gcz=0;
            break;*/

            case '1':
            moc=300;
            break;
            
            case '2':
            moc=400;
            break;

            case '3':
            moc=600;
            break;

            case '4':
            moc=800;
            break;
            
            case '5':
            moc=1000;
            break;
            
            case 'B':
            case 'b':
            moc=0;
            break;

            case 'W':
            case 'w':
            moc=moc+100;
            break;

            case 'S':
            case 's':
            moc=moc-100;
            break;

            case 'A':
            case 'a':
            if(kir>=-500+cnt)
            {
            kir=kir-50;
            
            }
            break;
    
            case 'D':
            case 'd':
            if(kir<=500+cnt)
            {
            kir=kir+50;
            }
            break;

            case 'U':
            case 'u':
            
            wysp=wysp-100;
            
            break;

            case 'J':
            case 'j':
            
            wysp=wysp+100;
            
            break;

            case 'I':
            case 'i':
            
            wyst=wyst-100;
            
            break;

            case 'K':
            case 'k':
           
            wyst=wyst+100;
            
            break;

            case 'H':
            case 'h':
            kir=0+cnt;
            break;

            case 'C':
            case 'c':
            if(str)
            str=0;
            else
            str=1;
            break;
            
            case 'N':
            case 'n':
            cnt=hMot2.getEncoderCnt();

            break;

            case 'G':
            case 'g':

            if(poz)
            {
            poz=0;
            printf("poz OFFss");
            }
            else
            {
            poz=1;
            printf("poz ON");
            }
            break;
        }
        
///////////////////////////////////////////////////////////////////////
        //AUTOMATYCZNE STEROWANIE
        if(str)
        {
            if(!stop(df))
            {
                if(!stop2)
                {
                    if(!ws)
                    {
                    hMot2.rotAbs(0, 500, true, INFINITE);
                    ws=1;
                    }
                    else
                    hMot2.rotAbs(kir, 500, true, INFINITE);                 
                    hMot1.setPower(moc);
                }
                else
                {
                    hMot1.setPower(0);
                    hMot2.rotAbs(-500, 500, true, INFINITE);
                    hMot1.setPower(300);
                    sys.delay(1000);
                }
            }
            else 
            {
                if(!stop2)
                {
                    hMot1.setPower(0);
                    ws=1;
                }
                else
                {
                    hMot1.setPower(-100);
                }
            }
            stop2=stop(df);
        }
        else
        {
            hMot1.setPower(moc);
            hMot2.rotAbs(kir, 500, false, INFINITE);
            hMot3.rotAbs(wysp, 1000, false, INFINITE);
            hMot4.rotAbs(wyst, 1000, false, INFINITE);
        }

        //printf("\n");
        //Serial.printf("stop = %d / stop2 = %d \r\n",stop(df),stop2);
/*
        if(ground(dg))
        {

        }
*/      

            
        c = '0';

        sys.delay(100);
    }
}